using Configs;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GameConfig))]
public class GameConfigCustomEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var config = (GameConfig)target;

        GUILayout.Space(20);
        
        if (GUILayout.Button("Find player config"))
        {
            config.FindPlayerConfig();
        }
    }
}
