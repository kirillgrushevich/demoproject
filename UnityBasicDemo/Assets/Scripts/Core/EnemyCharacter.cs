using System;
using Configs;
using UnityEngine;

namespace Core
{
    public class EnemyCharacter : Character
    {
        private enum State
        {
            Idle,
            Move,
            StartAttack,
            Attack,
            Die,
        }

        private State state;
        private readonly Character targetPlayer;

        private float attackCooldown;

        public EnemyCharacter(CharacterConfig config, Vector3 position, Character player) : base(config)
        {
            characterAnimator.transform.position = position;
            characterAnimator.transform.localEulerAngles = new Vector3(0f, -180f, 0f);

            targetPlayer = player;
        }

        public override void Update()
        {
            base.Update();

            switch (state)
            {
                case State.Idle:
                    DoIdle();
                    break;
                
                case State.Move:
                    DoMove();
                    break;
                
                case State.StartAttack:
                    StartAttack();
                    break;
                
                case State.Attack:
                    DoAttack();
                    break;
                
                case State.Die:
                    break;
                
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        
        private void DoIdle()
        {
            if (Mathf.Abs(targetPlayer.Transform.position.z - Transform.position.z) > 10f)
            {
                return;
            }

            state = State.Move;
        }
        
        private void DoMove()
        {
            if (Mathf.Abs(targetPlayer.Transform.position.z - Transform.position.z) > 1f)
            {
                MoveCharacter(Vector3.forward);
                return;
            }

            state = State.StartAttack;
            characterAnimator.SetFloat(SpeedFloat, 0);
        }
        
        private void StartAttack()
        {
            characterAnimator.SetTrigger(AttackTrigger);
            state = State.Attack;
            attackCooldown = Time.time + characterConfig.AttackCooldown;
        }

        private void DoAttack()
        {
            if (Time.time < attackCooldown)
            {
                return;
            }

            state = State.StartAttack;
        }
    }
}