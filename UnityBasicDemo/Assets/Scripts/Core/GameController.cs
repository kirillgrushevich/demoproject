using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Camera;
using Configs;
using Unity.VisualScripting;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core
{
    public class GameController
    {
        private SaveDataController saveDataController;
        private readonly UnityFunctionsEvents unityEvents;

        private List<Character> characters;
        
        public GameController(SaveDataController saveDataController)
        {
            this.saveDataController = saveDataController;

            unityEvents = new GameObject("UnityFunctionsEvents").AddComponent<UnityFunctionsEvents>();
            unityEvents.enabled = false;
            
            Object.DontDestroyOnLoad(unityEvents.GameObject());
        }

        public async Task PlayGame(Character playerCharacter, Character enemyCharacter)
        {
            var camera = Object.FindObjectOfType<GameCamera>();//TODO
            
            await Task.Delay(TimeSpan.FromSeconds(GameConfig.InitializationTime));

            characters = new List<Character>
            {
                playerCharacter,
                enemyCharacter,
            };

            camera.Setup(playerCharacter.Transform);
            
            unityEvents.OnUpdate += UpdateCharacters;
            unityEvents.enabled = true;
            await Task.Delay(TimeSpan.FromSeconds(GameConfig.GamePlayTime));

#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                return;
            }  
#endif

            unityEvents.enabled = false;
            unityEvents.OnUpdate -= UpdateCharacters;
        }

        private void UpdateCharacters()
        {
            foreach (var character in characters)
            {
                character.Update();
            }
        }

    }
}