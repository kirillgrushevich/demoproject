using System;
using UnityEngine;

namespace Core
{
    public class UnityFunctionsEvents : MonoBehaviour
    {
        public event Action OnUpdate;
        
        private void Update()
        {
            OnUpdate?.Invoke();
        }
    }
}