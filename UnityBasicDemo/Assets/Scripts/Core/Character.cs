using Configs;
using UnityEngine;

namespace Core
{
    public class Character
    {
        protected readonly CharacterConfig characterConfig;
        protected readonly Animator characterAnimator;
        
        protected static readonly int SpeedFloat = Animator.StringToHash("Speed");
        protected static readonly int AttackTrigger = Animator.StringToHash("Attack");

        public Transform Transform => characterAnimator.transform;

        protected Character(CharacterConfig config)
        {
            characterConfig = config;
            characterAnimator = Object.Instantiate(config.Animator);
        }
        
        public virtual void Update()
        {
            
        }

        protected void MoveCharacter(Vector3 direction)
        {
            characterAnimator.SetFloat(SpeedFloat, characterConfig.MovementSpeed);
            characterAnimator.transform.Translate(characterConfig.MovementSpeed * Time.deltaTime * direction);
        }
    }
}