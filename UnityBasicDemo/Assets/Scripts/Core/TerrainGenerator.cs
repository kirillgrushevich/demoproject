using Configs;
using UnityEngine;

namespace Core
{
    public class TerrainGenerator
    {
        public void GenerateTerrain(float sizeX, float sizeZ)
        {
            var config = TerrainConfig.Config;
            var data = config.GetTerrainTexture(TerrainType.Sandy);
            
            var obj = new GameObject("Terrain");
            var meshRenderer = obj.AddComponent<MeshRenderer>();
            var meshFilter = obj.AddComponent<MeshFilter>();

            var xUv = sizeX / data.size;
            var yUv = sizeZ / data.size;

            var mesh = new Mesh
            {
                vertices = new[]
                {
                    new Vector3(-sizeX, 0f, -sizeZ),
                    new Vector3(-sizeX, 0f, sizeZ),
                    new Vector3(sizeX, 0f, sizeZ),
                    new Vector3(sizeX, 0f, -sizeZ),
                },
                triangles = new[] { 0, 1, 2, 0, 2, 3 },
                uv = new Vector2[]
                {
                    new Vector3(0, 0f),
                    new Vector3(0, yUv),
                    new Vector3(xUv, yUv),
                    new Vector3(xUv, 0f),
                },
            };
            
            mesh.RecalculateBounds();

            meshFilter.sharedMesh = mesh;
            
            var material = new Material(Shader.Find("Universal Render Pipeline/Simple Lit"))
            {
                mainTexture = data.texture
            };

            meshRenderer.sharedMaterial = material;

        }
    }
}