using Configs;
using UnityEngine;

namespace Core
{
    public class PlayerCharacter : Character
    {
        public PlayerCharacter(CharacterConfig config) : base(config)
        {
        }

        public override void Update()
        {
            base.Update();
            
            if (Input.GetKey(KeyCode.W))
            {
                MoveCharacter(Vector3.forward);
            }
            else
            {
                characterAnimator.SetFloat(SpeedFloat, 0);
            }
        }
    }
}