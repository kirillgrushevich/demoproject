using System;
using System.Threading.Tasks;
using Configs;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core
{
    public static class MainController
    {
        private static GameController gameController;
        private static SaveDataController saveDataController;
        private static TerrainGenerator terrainGenerator;

        private static MainUiController mainUi;
        
        [RuntimeInitializeOnLoadMethod]
        public static async void InitGame()
        {
            saveDataController = new SaveDataController();
            gameController = new GameController(saveDataController);
            terrainGenerator = new TerrainGenerator();

            mainUi = new MainUiController();

            SceneManager.LoadScene(saveDataController.IsFirstRun ? 0 : 1);

            await Task.Delay(TimeSpan.FromSeconds(1f));
            
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                return;
            }
#endif

            var loader = SceneManager.LoadSceneAsync("MainScene");
            
            while (!loader.isDone)
            {
                await Task.Yield();
            }
            
            mainUi.Show(PlayGameCircle);
        }

        private static async void PlayGameCircle()
        {
            terrainGenerator.GenerateTerrain(20f, 30f);

            var player = new PlayerCharacter(GameConfig.PlayerCharacterConfig);
            var enemy = new EnemyCharacter(GameConfig.EnemyCharacterConfig, new Vector3(0f, 0f, 15f), player);
            
            await gameController.PlayGame(player, enemy);
            
            Debug.Log("Game over");
        }
    }
    
}