using UnityEngine;

namespace Core
{
    public class SaveDataController
    {
        public bool IsFirstRun
        {
            get
            {
                var isFirstRun = PlayerPrefs.GetInt("FirstRun", 0) == 0;
                if (isFirstRun)
                {
                    PlayerPrefs.SetInt("FirstRun", 1);
                }
                return isFirstRun;
            }
        }
    }
}