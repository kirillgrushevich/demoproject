using UnityEngine;

namespace Camera
{
    public class GameCamera : MonoBehaviour
    {
        [SerializeField] private Transform cameraRoot;
        
        private Transform target;

        public void Setup(Transform followTarget)
        {
            target = followTarget;
        }
        
        private void Update()
        {
            if (target == null)
            {
                return;
            }
            
            cameraRoot.transform.position = target.transform.position;
        }

#if UNITY_EDITOR
        [ContextMenu("Detach Camera")]
        private void DetachCamera()
        {
            target = null;
        }
#endif
        
    }
}
