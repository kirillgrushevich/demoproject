using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class MainUiView : MonoBehaviour
    {
        [Header("Buttons")]
        [SerializeField] private Button playButton;
        
        [Header("Test")]
        [SerializeField] private float test;

        public void Init(Action playCallback)
        {
            playButton.onClick.AddListener(delegate
            {
                playCallback?.Invoke();
            });
        }
    }
}