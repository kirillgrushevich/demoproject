using System;
using Unity.VisualScripting;
using UnityEngine;
using Object = UnityEngine.Object;

namespace UI
{
    public class MainUiController
    {
        private MainUiView view;

        private Action PlayEvent;

        public void Show(Action playCallback)
        {
            var data = Resources.Load<MainUiData>("Ui/MainUiData");
            view = Object.Instantiate(data.View);

            PlayEvent = playCallback;
            
            view.Init(OnPlayButton);
        }

        public void Hide()
        {
            if (view != null)
            {
                Object.Destroy(view.GameObject());
            }
        }

        private void OnPlayButton()
        {
            PlayEvent?.Invoke();
            Hide(); //TODO - call it from controller
            
        }
    }
}