using UnityEngine;

namespace UI
{
    [CreateAssetMenu(fileName = "Ui/MainUiData", menuName = "MainUiData", order = 2)]
    public class MainUiData : ScriptableObject
    {
        [SerializeField] private MainUiView view;

        public MainUiView View => view;
    }
}