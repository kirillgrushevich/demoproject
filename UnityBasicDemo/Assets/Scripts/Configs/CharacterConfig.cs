using Core;
using UnityEngine;

namespace Configs
{
    [CreateAssetMenu(fileName = "CharacterConfig", menuName = "Character config", order = 2)]
    public class CharacterConfig : ScriptableObject
    {
        [Header("Prefabs")]
        [SerializeField] private Animator animator;
        
        [Header("Main settings")]
        [SerializeField, Range(0.5f, 5f)] private float movementSpeed;
        [SerializeField] private float attackCooldown;


        public Animator Animator => animator;
        public float MovementSpeed => movementSpeed;

        public float AttackCooldown => attackCooldown;
    }
}