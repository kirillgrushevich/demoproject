using System;
using System.Linq;
using UnityEngine;

namespace Configs
{
    public enum TerrainType
    {
        BrownStony,
        Grass,
        Sandy,
    }
    
    [CreateAssetMenu(fileName = "TerrainConfig", menuName = "TerrainConfig", order = 1)]
    public class TerrainConfig : ScriptableObject
    {
        [Serializable]
        private class GroundPart
        {
            public TerrainType type;
            public Texture2D texture;
            public float worldSize;
        }

        [SerializeField] private GroundPart[] parts;

        public static TerrainConfig Config => Resources.Load<TerrainConfig>("TerrainConfig");//TODO ???

        public (Texture2D texture, float size) GetTerrainTexture(TerrainType type)
        {
            var part = parts.FirstOrDefault(p => p.type == type);
            return new ValueTuple<Texture2D, float>(part?.texture, part?.worldSize ?? 2f);

        }
    }
}