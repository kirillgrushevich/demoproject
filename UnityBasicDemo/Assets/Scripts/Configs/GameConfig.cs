#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;

namespace Configs
{
    [CreateAssetMenu(fileName = "GameConfig", menuName = "GameConfig", order = 0)]
    public class GameConfig : ScriptableObject
    {
        [Header("Game settings")]
        [SerializeField] private float initializationTime;
        [SerializeField] private float gamePlayTime;


        [Header("Characters configs")] 
        [SerializeField] private CharacterConfig playerCharacterConfig;
        [SerializeField] private CharacterConfig enemyCharacterConfig;

        private static GameConfig instance;
        private static GameConfig Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = Resources.Load<GameConfig>("GameConfig");
                }
                return instance;
            }
        }

        public static float InitializationTime => Instance.initializationTime;
        public static float GamePlayTime => Instance.gamePlayTime;
        

        public static CharacterConfig PlayerCharacterConfig => Instance.playerCharacterConfig;
        public static CharacterConfig EnemyCharacterConfig => Instance.enemyCharacterConfig;

#if UNITY_EDITOR
        [ContextMenu("Find player config")]
        public void FindPlayerConfig()
        {
            playerCharacterConfig = AssetDatabase.LoadAssetAtPath<CharacterConfig>(@"Assets/Resources/Characters/PlayerCharacter.asset");
        }
#endif
    }
}